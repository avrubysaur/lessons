<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
IncludeTemplateLangFile(__FILE__);


//pre($arResult);
?>
<!-- Portfolio Grid Section -->
<section class="portfolio" id="portfolio">
    <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0"><?=GetMessage("PORTFOLIO_TITLE");?></h2>
        <hr class="star-dark mb-5">
        <div class="row">

            <?foreach ($arResult["ITEMS"] as $key => $item):?>

                <div class="col-md-6 col-lg-4">
                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-<?=$key?>">
                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                                <i class="fas fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img class="img-fluid" src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$item["PREVIEW_PICTURE"]["ALT"]?>">
                    </a>
                </div>

            <?endforeach;?>

        </div>
    </div>
</section>

<?foreach ($arResult["ITEMS"] as $key => $item):?>
    <!-- Portfolio Modal -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-<?=$key?>">
        <div class="portfolio-modal-dialog bg-white">
            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i class="fa fa-3x fa-times"></i> </a>
            <div class="container text-center">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <h2 class="text-secondary text-uppercase mb-0"><?=$item["NAME"]?></h2>
                        <hr class="star-dark mb-5">
                        <img src="<?=$item["DETAIL_PICTURE"]["SRC"]?>" class="img-fluid mb-5" alt="<?=$item["DETAIL_PICTURE"]["ALT"]?>">
                        <p class="mb-5">
                            <?=$item["~DETAIL_TEXT"]?>
                        </p>
                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i class="fa fa-close"></i>
                            <?=GetMessage("PORTFOLIO_CLOSE");?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>