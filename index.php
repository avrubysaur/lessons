<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Главная"); ?>

    <!— Начало станицы —>
    <div class="masthead bg-primary text-white text-center" style="margin-top: 105px">
        <div class="container">
            <img src="<?= SITE_TEMPLATE_PATH ?>/img/profile.png" class="img-fluid mb-5 d-block mx-auto" alt="">
            <h1 class="text-uppercase mb-0">Start Bootstrap</h1>
            <hr class="star-light">
            <h2 class="font-weight-light mb-0">Web Developer - Graphic Artist - User Experience Designer</h2>
        </div>
    </div>
    <!— Иконки с портфолио —>
    <section class="portfolio" id="portfolio">
        <div class="container">
            <h2 class="text-center text-uppercase text-secondary mb-0">Portfolio</h2>
            <hr class="star-dark mb-5">

            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("", ""),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => $_REQUEST["ID"],
                    "IBLOCK_TYPE" => "news",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("", ""),
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N"

                )

            ); ?>

            <!--            <div class="row">-->

            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-1">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!---->
            <!---->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/cabin.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-2">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/cake.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-3">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/circus.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-4">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/game.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-5">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/safe.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--                <div class="col-md-6 col-lg-4">-->
            <!--                    <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-6">-->
            <!--                        <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">-->
            <!--                            <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">-->
            <!--                                <i class="fas fa-search-plus fa-3x"></i>-->
            <!--                            </div>-->
            <!--                        </div>-->
            <!--                        <img src="-->
            <? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/submarine.png" class="img-fluid" alt=""> </a>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->
    </section>
    <!— Секция "About" —>
    <section class="bg-primary text-white mb-0" id="about">
        <div class="container">
            <h2 class="text-center text-uppercase text-white">About</h2>
            <hr class="star-light mb-5">
            <div class="row">
                <div class="col-lg-4 ml-auto">
                    <p class="lead">
                        Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the
                        complete
                        source files including HTML, CSS, and JavaScript as well as optional LESS stylesheets for easy
                        customization.
                    </p>
                </div>
                <div class="col-lg-4 mr-auto">
                    <p class="lead">
                        Whether you're a student looking to showcase your work, a professional looking to attract
                        clients,
                        or a graphic artist looking to share your projects, this template is the perfect starting point!
                    </p>
                </div>
            </div>
            <div class="text-center mt-4">
                <a class="btn btn-xl btn-outline-light" href="#"> <i class="fas fa-download mr-2"></i>
                    Download Now! </a>
            </div>
        </div>
    </section>
    <!— Секция с контактами —>
    <section id="contact">
        <div class="container">
            <h2 class="text-center text-uppercase text-secondary mb-0">Contact Me</h2>
            <hr class="star-dark mb-5">
            <div class="row">
                <div class="col-lg-8 mx-auto">
                    <!— To configure the contact form email address, go to mail/contact_me.php and update the email
                    address in the PHP file on line 19. —>
                    <!— The form should work on most web servers, but if the form is not working you may need to
                    configure your web server differently. —>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Name</label> <input class="form-control" id="name" type="text" placeholder="Name"
                                                           required="required"
                                                           data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger">
                                </p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Email Address</label> <input class="form-control" id="email" type="email"
                                                                    placeholder="Email Address" required="required"
                                                                    data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger">
                                </p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Phone Number</label> <input class="form-control" id="phone" type="tel"
                                                                   placeholder="Phone Number" required="required"
                                                                   data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger">
                                </p>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                <label>Message</label> <textarea class="form-control" id="message" rows="5"
                                                                 placeholder="Message" required="required"
                                                                 data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger">
                                </p>
                            </div>
                        </div>


                        <div id="success">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!— Начало скриптов —> <!— Scroll to Top Button (Only visible on small and extra-small screen sizes) —>
    <div class="scroll-to-top d-lg-none position-fixed ">
        <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"> <i
                    class="fa fa-chevron-up"></i> </a>
    </div>
    <!--    <!— Portfolio Modals —> <!— Portfolio Modal 1 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-1">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/cabin.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <!— Portfolio Modal 2 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-2">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/cake.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <!— Portfolio Modal 3 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-3">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/circus.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <!— Portfolio Modal 4 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-4">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/game.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <!— Portfolio Modal 5 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-5">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/safe.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--    <!— Portfolio Modal 6 —>-->
    <!--    <div class="portfolio-modal mfp-hide" id="portfolio-modal-6">-->
    <!--        <div class="portfolio-modal-dialog bg-white">-->
    <!--            <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i-->
    <!--                        class="fa fa-3x fa-times"></i>-->
    <!--            </a>-->
    <!--            <div class="container text-center">-->
    <!--                <div class="row">-->
    <!--                    <div class="col-lg-8 mx-auto">-->
    <!--                        <h2 class="text-secondary text-uppercase mb-0">Project Name</h2>-->
    <!--                        <hr class="star-dark mb-5">-->
    <!--                        <img src="--><? //= SITE_TEMPLATE_PATH ?><!--/img/portfolio/submarine.png" class="img-fluid mb-5" alt="">-->
    <!--                        <p class="mb-5">-->
    <!--                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam-->
    <!--                            nihil,-->
    <!--                            molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae?-->
    <!--                            Reprehenderit-->
    <!--                            soluta, eos quod consequuntur itaque. Nam.-->
    <!--                        </p>-->
    <!--                        <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#"> <i-->
    <!--                                    class="fa fa-close"></i>-->
    <!--                            Close Project</a>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!— Bootstrap core JavaScript —>
    <script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!— Plugin JavaScript —>
    <script src="<?= SITE_TEMPLATE_PATH ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!— Contact Form JavaScript —>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jqBootstrapValidation.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/contact_me.js"></script>
    <!— Custom scripts for this template —>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/freelancer.min.js"></script><? require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');

?>